const _ = require('lodash');
const jwt = require('jsonwebtoken');

/**
 * User mapper
 * @class UserMapper
 */
class UserMapper {

  /**
   * Responses
   */
  static getUserInfoToResponse(user) {
    if (!user) return ({message: 'User isn`t found!'})
    return {
      name: user.username,
      email: user.email
    }
  }
}

module.exports =  UserMapper;