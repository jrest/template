const express = require('express')
    , path = require('path')
    , cookieParser = require('cookie-parser')
    , bodyParser = require('body-parser')
    , index = require('./routers/index')
    , logger = require('morgan')
    , http = require('http')
    , expressValidator = require('express-validator');

const app = express();

const port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

app.use(bodyParser.json({limit: 1024 * 1024 * 20}))
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(expressValidator())

app.use('/', index);
/**
 * Error handler
 */
app.use((err, req, res, next) => {
  res.status(err.status || 500)
  res.json({
    status: err.status || 500,
    message: err.message,
    errors: err.errors
  })
})

const server = http.createServer(app);
server.listen(port, (err) => {
    if (err) {
        return console.log('Server off-line', err)
    }

    console.log(`Server is listening on port ${port}`);
});


function normalizePort(val) {
    const port = parseInt(val, 10);

    if (isNaN(port)) {
        return val;
    }

    if (port >= 0) {
        return port;
    }

    return false;
}

module.exports = app;
