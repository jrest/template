'use strict'

const async = require('async')
const bcrypt = require('bcryptjs')

const SALT = 10

class Utils {
    /**
     * Generate hash for password
     *
     * @param password
     * @param callback
     */
  static generateHash (password) {
    bcrypt.genSalt(SALT)
      .then((salt) => bcrypt.hash(password, salt, (err, hash) => {
        return(hash, salt)
    }))
  }
}

module.exports = Utils
