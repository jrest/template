const config = require('../config/index')
const DaoFactory = require('../dao/index');
const userService = require('./users-service');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
/**
 * Auth service
 * @class UserService
 */
class UserService {

  constructor() {
      this._usersDao = DaoFactory.loadDao('user-dao');
  }

  /**
   * Get user info
   * @param {Object} params
   * @return {Promise.<Object>}
   */
  singin(params) {
    return userService.getUserCred(params.email)
      .then((user) => {
        if (!user)
          return ({err: "Error while signing in. Please, check your email or try to sing up"})
        if (!bcrypt.compareSync(params.password, user.password)) {
          return ({err: 'Incorrect password'})
        }
        return ({
          message: "You successfuly signed in!",
          email: user.email,
          authToken: jwt.sign({id: user.userId, email: user.email}, config.secretKey, {expiresIn: 7200000})
        })
      })
  }

  signup(params) {
    return userService.createUser(params)
    .then(user => {
      let userData = {
        userId: user.id,
        email: user.email,
        password: params.password
      }
      userService.createUserCred(userData)
        return ({message: "You are successfuly signed up!"})
      })
    }
}

module.exports = new UserService();