const DaoFactory = require('../dao/index');
const bcrypt = require('bcryptjs');

/**
 * User service
 * @class UserService
 */
class UserService {

  constructor() {
    this._usersDao = DaoFactory.loadDao('user-dao');
  }

  /**
   * Get user info
   * @param {Object} params
   * @return {Promise.<Object>}
   */
  getUserInfo(params) {
    return this._usersDao.getUsers(params);
  }

  getUserCred(params) {
    return this._usersDao.getUserCred(params);
  }

  createUser(params) {
    return this._usersDao.createUser(params);
  }

  createUserCred(params) {
    bcrypt.genSalt(10)
      .then(salt => {
        params.salt = salt
        return bcrypt.hash(params.password, salt)
      })
      .then(password => {
        params.password = password
        return this._usersDao.createUserCred(params);
      })
  }

  createCourse(params) {
    return this._usersDao.createCourse({
      userId: params.userId,
      courseName: params.courseName,
      dateFrom: params.dateFrom,
      dateTo: params.dateTo,
      maxMembers: params.maxMembers
    })
  }
}

module.exports = new UserService();