const db = require('../../models/postgres/index');

/**
 * User Dao
 * @class UserDao
 */
class UserDao {
    /**
     * Get user
     * @param {Object} params
     * @return {Promise.<Object>}
     */
  getUsers(params) {
    return db.Users.findOne({
      where: params
    })
    .catch((err) => {
      throw new Error(err.message);
    });
  }

  /**
   * Create test user
   *
   * @param params
   * @returns {*|Promise<T>}
   */
  createUser(params) {
    return db.Users.create({
      email: params.email,
      username: params.username,
      roleId: params.roleId
    })
      .catch((err) => {
        throw new Error(err.message);
      });
  }
}

module.exports = new UserDao();