const config = require('../config/index');

class DaoFactory {
    loadDao(daoName) {
        return require(`./api/dao`);
    }
}

module.exports = new DaoFactory();
