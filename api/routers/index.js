const express = require('express')
    , app = express()
    , index = require('./v1/index');

app.use('/api/v1', index);

module.exports = app;
