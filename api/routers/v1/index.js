const express = require('express')
    , app = express()
    , users = require('./users-router')
    , login = require('./auth-router');

app.use('/', login);
app.use('/user', users);

module.exports = app;
