const express = require('express')
const authService = require('../../services/auth-service')
const badRequestError = require('../../errors/bad-request');
const router = express.Router()
const jwt = require('jsonwebtoken')
const config = require('../../config/index')

/**
 * Auth router
 */
router.use('/*', (req, res, next) => {
  req.user = jwt.decode(req.headers.token)
  next()
})

/**
 * User authorization
 */
router.post('/auth', (req, res, next) => {
  req.checkBody('email', 'Email must be provided').notEmpty()
  req.checkBody('password', 'Password must be provided').notEmpty()
  let errors = req.validationErrors()

  if (errors) return res.status(401).json(errors)
    authService.singin(req.body)
      .then((user) => {
        if (user.err)
          return res.status(403).json(user.message)
        return res.status(200).json(user)
      })
      .catch ((err) => {
        return next(new badRequestError(err.message))
      })
})

/**
 * User registration
 */
router.post('/register', (req, res, next) => {
  req.checkBody('email', 'Email is required').notEmpty()
  req.checkBody('username', 'Username is required').notEmpty()
  authService.signup(req.body)
    .then(user => {
      if (!user) return next('Error while singing up')
      res.json({
        message: "You are successfuly signed up!"
      })
    })
    .catch((err) => {
      return next(new badRequestError(err.message))
    })
})

router.use('/*', (req, res, next) => {
  try {
    jwt.verify(req.headers.token, config.secretKey)
    if (!req.user) return res.status(401).json({
      message: "You are not authorized!"
    })
    return next()
  }catch (err) {
    if (err) return res.status(401).json({
        message: "You are not authorized!"
      })
  }
})
module.exports = router

