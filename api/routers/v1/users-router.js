const express = require('express');
const userMapper = require('../../mappers/user-mapper');
const userService = require('../../services/users-service');
const badRequestError = require('../../errors/bad-request');
const router = express.Router();
const _ = require('lodash')

/**
 * Get user profile
 */
router.get('/profile', (req, res, next) => {
  req.checkQuery('username', 'Username must be provided').notEmpty()
  let errors = req.validationErrors()
  if (errors) return next(new badRequestError(errors))

    return userService.getUserInfo(req.query)

      .then((user) => {
        res.json(userMapper.getUserInfoToResponse(user))
      })
      .catch ((err) => {
        return next(new badRequestError(err.message))
      })
})

/**
 * Create course
 */
router.post('/course', (req, res, next) => {
  req.checkBody('courseName', 'Course name must be provided').notEmpty()
  req.checkBody('dateFrom', 'Date of starting the course must be provided').notEmpty()
  req.checkBody('dateTo', 'Date of ending the course must be provided').notEmpty()
  let errors = req.validationErrors()

  // if (errors) return next(new badRequestError(errors))
  if (errors)
    _.forEach(errors, error => {
      return next(new badRequestError(error.msg))
    })
  let params = {
    userId: req.user.id,
    courseName: req.body.courseName,
    dateFrom: req.body.dateFrom,
    dateTo: req.body.dateTo,
    maxMembers: req.body.maxMembers
  }
  return userService.createCourse(params)
    .then((course) => {
    if (!course) return next('Oops! Error while creating course! Please, try again later')
      res.json({
        message: 'Course was created successful!',
        courseName  : course.courseName,
        dateStart: course.dateFrom,
        dateEnd: course.dateTo,
        maxMembers: course.maxMembers
      })
    })
    .catch ((err) => {
      if (err.message === 'Validation error')
        return next(new badRequestError('Course with this name was already created!'))
    })
})


module.exports = router