'use strict'

class BadRequestError extends Error {
  constructor (errors) {
    const message = 'Bad request'
    super(message)
    this.status = 400
    this.message = message
    this.errors = errors
  }
}

module.exports = BadRequestError
