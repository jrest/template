module.exports = {
    env: process.env.NODE_ENV || 'development',
    root: `${__dirname}/../../../`,
    host: process.env.HOST || 'http://localhost',
    port: process.env.PORT || 3000,
    secretKey: process.env.SECRET || 'Whiskey in the jar',
    dialect: process.env.dialect || 'postgres',

    database: {
        host: process.env.DB_HOST || 'localhost',
        port: process.env.DB_PORT || '5432',
        name: process.env.DB_NAME || 'msadb',
        user: process.env.DB_USER || 'postgres',
        password: process.env.DB_PASSWORD || 'password',
    },

    cors: {
        credentials: true,
        allowMethods: ['GET', 'POST', 'PUT', 'DELETE']
    }
};
