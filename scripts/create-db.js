const { Client } = require('pg');

const pgConf = require('../api/config');

const CONNECTION_STRING = `postgres://${pgConf.database.user}:${pgConf.database.password}@${pgConf.database.host}:${pgConf.database.port}/postgres`;
const client = new Client(CONNECTION_STRING)

client.connect();
console.log('DB connection string - ', CONNECTION_STRING)
client.query(`CREATE DATABASE ${pgConf.database.name}`, (err) => {
    console.log('----', err);
    client.end();
});

