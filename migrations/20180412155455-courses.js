'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Courses', {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      courseName: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      dateFrom: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      dateTo: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      maxMembers: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Date.now()
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Date.now()
      }
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Courses');
  }
};
