const pgConf = require('../../api/config');

module.exports = {
    [pgConf.env]: {
        username: pgConf.database.user,
        password: pgConf.database.password,
        database: pgConf.database.name,
        host: pgConf.database.host,
        port: pgConf.database.port,
        dialect: 'postgres'
    }
};